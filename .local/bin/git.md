# Custom Git functions for Agile workflow

The Git functions described below intend to ease development according to the following workflow:

1. Start a work branch off `master` (or any other Git branch, such as a release branch).
2. Work in the branch, while including new developments from `master` as often as possible. The work branch must have a very short lifetime and it is good practice to have a 1:1 relationship between work branches and Product Backlog items.
3. When the development is finished, tested (potentially peer reviewed), it is merged back to its original branch and the work branch can be deleted.

For this purpose, I use the following functions:
- `git start` to create the work branch
- `git update` to include changes from the original branch (as many times as necessary)
- `git finish` to merge the work into the original branch *in a single commit* and delete the work branch
- `git delete` to delete a work branch


## Simple example

The following details the basic workflow.

```
      master       origin/master
 *start |              |   ^
        |              |   |
        |              |   |
        v      *update v   | *finish
         US001-Login-screen
                 |
                 | `git commit`
                 | `git push`
                 v
      origin/US001-Login-screen
```


From branch master, start a new work:

```
git start US001-Login-screen
```

You are automatically placed in working branch,
you can work on it, commiting and pushing as usual:

```
[...]
git commit ...
git push
[...]
```

From time to time, pull new developments from
the team into your local master branch and
rebase your work branch with a single command:

```
git update
```

When your work is complete, push it to master
as a single commit and delete your work branch:

```
git finish
```

You are back on master.


## Multi-layer example

Although rarely justified because the work branch is really intended to represent a small amount of work, these Git commands allow sub-branches as well. This can be used for example if one wants to experiment a way of doing things, while still making as many commits as necessary, and be able to easily revert to the previous state just by deleting the experimentation branch.

```
      master            ------ origin/master -----
  *start |              |            |           ^
         |              |            |           |
         |              |            |           |
         v              v *update    |           | *finish
         ----------- US001-Login-screen ----------
          |               | *start   |          ^
          | `git commit`  |          |          |
          | `git push`    |          |          |
          v               |          |          |
origin/US001-Login-screen |          |          | 
                          v          v *update  | *finish
                          ----- Experiment1 -----
                                     |
                                     | `git commit`
                                     | `git push`
                                     v
                            origin/Experiment1
```

From branch master, start a new work:

```
git start US001-Login-screen
```

Work on the new branch. Whenever you feel you reached a critical point
where you think you may have to revert later on, create a new work branch:

```
git start Experiment1
```

At any point, you can update your local work from master and through
the first working branch with a single `git update`.

If you finally want to cancel your experiment, just `git delete`.

If you want to include it in its parent work branch, just `git finish`.
And if this complete the whole work, `git finish` it again to push it
to master.


## Configuration

The following git options can modify the behavior of these scripts:

- `custom.syncwithremote`

When set to `true`, updates are fetched from the remote server and
branch creations/deletions are automatically synchronized to the remote
server.

- `custom.pushonfinish`

When set to `true`, the command `git finish` automatically pushes
the merged branch to the remote server.

Setting these two options to `false` (the default) assures a completely
disconnected operation mode.


# Other custom Git commands

`git graph` is a `git log` replacement showing information in a more graphical way,
although still in console mode.

`git switchbranch` intends to slightly ease branch switching by displaying all
candidates and allow selecting them by number. This command becomes especially
convenient when using above Git functions for Agile workflow because of how
child branches are named.

`git recursive <git command and args>` allows executing any git command recursively
in all Git repositories located in the current directory and its sub-directories.
This command may be convenient in situations where a large project is split in
several repositories. E.g. `git recursive status`.


# Licensing

```
MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
