#!/bin/bash
#
# This script deletes a branch, locally and remotely.
#
# Features:
# - Do not delete a branch if it has children
# - Delete the current branch if no name is specified on command-line
# - Multiple branch deletions with a simple call
#
# Author: Konnichy <katakayann@gmail.com>
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. git.functions


# Check command-line parameters
# -----------------------------
script=$(basename $0)
current_branch=$(get_current_branch)

if [ $# -eq 0 ]; then
    branches_to_delete=$current_branch
else
    branches_to_delete="$*"
fi


# Delete each branch specified on command-line
# --------------------------------------------
for branch_to_delete in $branches_to_delete; do
    confirm "Do you want to delete branch '$branch_to_delete'"
    if [ $? -eq 0 ]; then
        delete_branch $branch_to_delete
    fi
done
