#!/bin/sh

if [ "$1" = "build" ]; then
    echo "Building a cscope database..."
    find . -name 'cscope.*' | xargs rm -f
    find . -type f -name '*.[ch]' | grep -v "^\./tmp/" > cscope.files
    cscope -b -q -k
elif [ "$1" = "update" ]; then
    echo "Updating cscope database..."
    find . -type f -name '*.[ch]' | grep -v "^\./tmp/" > cscope.files
    cscope -b -q -k
else
    echo "Usage:"
    echo "    $0 build  : Build a database in the current directory"
    echo "    $0 update : Update the database present in the current directory"
    exit 1
fi

exit 0
