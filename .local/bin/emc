#!/bin/bash
#
# Open a file from command-line into a currently running Emacs session
#
# Author: Konnichy <katakayann@gmail.com>
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

for argument in ${@}; do
    # Transform from grep format: <filename>:<linenumber>
    # to Emacs format: +<linenumber> <filename>

    filename=$(echo $argument | sed --regexp-extended 's/(.*):[0-9]*$/\1/')
    linenumber=$(echo $argument | sed --regexp-extended 's/.*:([0-9]*$)/\1/')
    if [ "$linenumber" != "$argument" ]; then
        # a line number was provided
        emacs_arguments="+$linenumber $filename"
    else
        # no line number was provided
        emacs_arguments="$filename"
    fi

    # Open files with multiple calls to Emacs Client to stack them in order.
    # I.e. using 'CTRL-x b' (switch buffer) or 'CTRL-x k' (kill buffer)
    # predictively switches to the next file in line, while opening all of
    # them with a single call to emacsclient does not.
    emacsclient -n $emacs_arguments
done
