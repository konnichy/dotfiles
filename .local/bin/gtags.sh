# Prerequisites:
#   sudo apt install python-pygments
#   sudo apt install exuberant-ctags
#   zcat /usr/share/doc/global/examples/gtags.conf.gz > ~/.gtags.conf

find . \( -name '*.[ch]' -o -name '*.py' \) \( -type f -o \( -type l ! -xtype l ! -xtype d \) \) > GTAGS.files
gtags -f GTAGS.files
