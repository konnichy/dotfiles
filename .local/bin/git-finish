#!/bin/bash
#
# This script merges a work branch back into its parent branch in a single commit.
#
# Features:
# - Trigger conflict resolution in the work branch to assure a successful merge
# - Squash work branch commits into a single one when merging to keep a clean
#   history on the parent branch
# - Pushes code on the remote if 'git config custom.pushonfinish' is set to 'true'
# - Asks whether to delete the merged branch or not
#
# Author: Konnichy <katakayann@gmail.com>
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

. git.functions


# Check command-line parameters
# -----------------------------
script=$(basename $0)
if [ $# -gt 1 ]; then
    echo "Usage: $script [<commit message>]"
    exit 1
fi
if [ $# -eq 1 ]; then
    commit_message=$1
else
    commit_message=""
fi

current_branch=$(get_current_branch)
parent_branch=$(parent_branch $current_branch)


cd "$(toplevel_directory)"
. "$(git --exec-path)/git-sh-setup"
require_clean_work_tree || exit $?


# Ask the user for confirmation
# -----------------------------
confirm "Do you really want to merge this branch into '$parent_branch'" --cancel


# Build a default commit message from current branch's commits
# ------------------------------------------------------------
if [ "$commit_message" = "" ]; then
    commit_message="$(git log --pretty='format:%B' $parent_branch..)"
fi


# Update from parent to trigger
# conflict resolution before merging
# ----------------------------------
if [ "$(git config custom.syncwithremote)" = "true" ]; then
    git pull --rebase origin $parent_branch || exit $?
    # (instead of 'git fetch' + 'git rebase':
    # http://gitolite.com/git-pull--rebase.html)
else
    git rebase $parent_branch
fi


# Save the last commit in the parent branch to be able
# to undo the merge if the commit is cancelled by the user
# --------------------------------------------------------
git checkout $parent_branch || exit $?
last_commit=$(git log -1 --pretty=format:%H HEAD)


# Squash commits into the parent branch
# -------------------------------------
git merge --ff --squash $current_branch || exit $?
# (--ff is necessary because --squash is not compatible with --no-ff)
git commit --edit --message "$commit_message"
if [ $? -ne 0 ]; then
    # commit failed or was cancelled by user: reset to the previous state
    # read: - https://stackoverflow.com/questions/14308580/undo-a-fast-forward-merge
    #       - https://stackoverflow.com/questions/134882/undoing-a-git-rebase
    git reset --hard $last_commit
    git checkout $current_branch
    exit 1
fi
if [ "$(git config custom.pushonfinish)" = "true" ]; then
    git push origin $parent_branch || exit $?
fi


# Delete the merged branch
# ------------------------
delete_branch $current_branch
