# My dotfiles

This is the place where I store dotfiles I want synchronized on different machines. Everyone is free to download them and use them according to their individual license terms.

Notes in this README file detail how to use the full repository or create your own one. I didn't invent anything though, this is an evolution of another person's evolution of [...] of another person's work.

Sources:

- https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/

- https://news.ycombinator.com/item?id=11070797

- https://news.ycombinator.com/item?id=11071976


## Initial creation of the repository

```
git init --bare $HOME/.git-home
git config --global alias.home '!git --git-dir=$HOME/.git-home/ --work-tree=$HOME'
git home config --local status.showUntrackedFiles no
```

From here, files can already be added/committed to the local repository. However, I wanted it stored remotely. Then I created a repository on BitBucket and wrote down its clone URL.

```
git home add <first_file_to_add>
git home commit <file_added_above>
git home config user.name "<name>"
git home config user.email "<email address>"
git home remote add origin https://konnichy@bitbucket.org/konnichy/dotfiles.git
git home push --set-upstream origin master
```


## Retrieve the repository on another machine

A part of the initial steps that were performed on the first machine must be done again on this one.

```
git clone --separate-git-dir=$HOME/.git-home https://konnichy@bitbucket.org/konnichy/dotfiles.git ~/.git-home.todelete
rm -r ~/.git-home.todelete
git config --global alias.home '!git --git-dir=$HOME/.git-home/ --work-tree=$HOME'
cd
git home checkout -- .
git home config --local status.showUntrackedFiles no
git home config user.name "<name>"
git home config user.email "<email address>"
```


## Usage

The new command `git home` can be used as simply as the regular `git`:

```
git home add <file>
git home commit -m "<message>" <file>
git home push
```
