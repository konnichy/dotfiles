# Environment variables
TERM=xterm-256color


# Set PATH to include custom scripts
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi


# Generic aliases
# alias gtags='GTAGSCONF=/usr/local/share/gtags/gtags.conf GTAGSLABEL=pygments gtags'
alias home='git home'
# alias dig='dig +noall +answer +multiline'
alias dig='dig +nocmd +noquestion +nocomments +nostats +multiline'


# Specific aliases
if [ -e $HOME/.bash_aliases.local ]; then
    . $HOME/.bash_aliases.local
fi


# Start Starship prompt
export STARSHIP_CONFIG=$HOME/snap/starship/common/starship.toml
eval "$(starship init bash)"


# Environment variables
# - Gtags
# GTAGSCONF=/usr/local/share/gtags/gtags.conf
# GTAGSLABEL=pygments
# - Ignore shell lines in history when they begin with a space
# (in addition to ignoring duplicates)
HISTCONTROL=ignoreboth


# Set custom prompt
# (with colors and current Git branch)
# Source: http://stackoverflow.com/questions/4133904/ps1-line-with-git-current-branch-and-colors
# Colors: 30=Black 31=Red 32=Green 33=Yellow 34=Blue 35=Magenta 36=Cyan 37=White
#         00 resets all attributes to default
function color_my_prompt {
    if [ -e /.dockerenv ]; then
        local __user_color="33m"
    else
        if [ "$USER" == "root" ]; then
            local __user_color="31m"
        else
            local __user_color="32m"
        fi
    fi
    local __user_and_host="\[\033[01;$__user_color\]\u@\h"
    # local __separator="\[\033[$__user_color\]\`for i in \$(seq 1 \$COLUMNS); do echo -n \"-\"; done\`\n"
    local __separator="\n"
    local __cur_location="\[\033[01;34m\]\w"
    local __git_branch_color="\[\033[35m\]"
    local __git_branch='`git branch 2> /dev/null | grep -e ^* | sed -E  s/^\\\\\*\ \(.+\)$/\(\\\\\1\)\ /`'
    local __prompt_tail="\[\033[01;$__user_color\]$"
    local __prompt_continue="\[\033[31m\]>"
    local __last_color="\[\033[00m\]"
#    export PS1="$__user_and_host $__cur_location $__git_branch_color$__git_branch$__prompt_tail$__last_color "

    # Separator + Standard Ubuntu prompt (colorized version) + the Git branch
    PS1="$__separator\${debian_chroot:+(\$debian_chroot) }$__user_and_host\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\] $__git_branch_color$__git_branch$__prompt_tail$__last_color "
    PS1="$__separator\${debian_chroot:+(\$debian_chroot) }$__user_and_host\[\033[00m\]:\n\[\033[01;34m\]\w\[\033[00m\] $__git_branch_color$__git_branch$__prompt_tail$__last_color "
    PS2="$__prompt_continue$__last_color "
}
color_my_prompt
