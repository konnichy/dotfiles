function home --wraps='git' --description "Shortcut for 'git home'"
    git home $argv
end
