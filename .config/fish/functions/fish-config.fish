function fish-config --wraps='fish_config' --description 'Open fish_config in a non-snap web browser'
    BROWSER=epiphany-browser fish_config $argv
end
