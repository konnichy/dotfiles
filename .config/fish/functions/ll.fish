#
# These are very common and useful
#
function ll --wraps ls --description "List contents of directory using long format"
    ls -l --human-readable --all --color $argv
end
