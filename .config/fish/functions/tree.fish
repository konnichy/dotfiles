function tree --wraps='tree' --description "Colorize tree's output by default"
    command tree -C $argv
end
